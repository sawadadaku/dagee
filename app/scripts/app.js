'use strict';

/**
 * @ngdoc overview
 * @name dageeViewApp
 * @description
 * # dageeViewApp
 *
 * Main module of the application.
 */
angular
  .module('dageeViewApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'ui.bootstrap'
  ])
  .config(function ($routeProvider,$stateProvider,$urlRouterProvider) {
            // $locationProvider.hashPrefix('!');
            // $locationProvider.html5Mode(true);
            $stateProvider.state("index", {
                url: "/",
                controller: "MainCtrl",
                controllerAs: "main",
                templateUrl: "views/main.html"
            }).state("about", {
                url: "/about",
                controller: "AboutCtrl",
                controllerAs: "about",
                templateUrl: "views/about.html"
            });
  });
