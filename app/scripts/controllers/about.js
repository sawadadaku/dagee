'use strict';

/**
 * @ngdoc function
 * @name dageeViewApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the dageeViewApp
 */
angular.module('dageeViewApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
