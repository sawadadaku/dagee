'use strict';

/**
 * @ngdoc function
 * @name dageeViewApp.controller:AreaFooterCtrl
 * @description
 * # AreaFooterCtrl
 * Controller of the dageeViewApp
 */
angular.module('dageeViewApp')
  .controller('AreaFooterCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
