'use strict';

/**
 * @ngdoc function
 * @name dageeViewApp.controller:AreaHeaderCtrl
 * @description
 * # AreaHeaderCtrl
 * Controller of the dageeViewApp
 */
angular.module('dageeViewApp')
  .controller('AreaHeaderCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
