'use strict';

/**
 * @ngdoc function
 * @name dageeViewApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the dageeViewApp
 */
angular.module('dageeViewApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
