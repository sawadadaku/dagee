'use strict';

/**
 * @ngdoc directive
 * @name dageeViewApp.directive:footer
 * @description
 * # footer
 */
angular.module('dageeViewApp')
  .directive('footer', function () {
    return {
      restrict: 'AE',
      templateUrl: "views/area/footer.html",
      controller: "AreaFooterCtrl",
      controllerAs: "fc",
      link: function postLink(scope, element, attrs) {
      }
    };
  });
