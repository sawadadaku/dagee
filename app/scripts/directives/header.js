'use strict';

/**
 * @ngdoc directive
 * @name dageeViewApp.directive:header
 * @description
 * # header
 */
angular.module('dageeViewApp')
  .directive('header', function () {
    	return {
            templateUrl: "views/area/header.html",
            restrict: 'AE',
            controller: "AreaHeaderCtrl",
            controllerAs: "hc",
            link: function postLink(scope, element, attrs) {

            }
        };
  });
