'use strict';

describe('Controller: AreaFooterCtrl', function () {

  // load the controller's module
  beforeEach(module('dageeViewApp'));

  var AreaFooterCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AreaFooterCtrl = $controller('AreaFooterCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(AreaFooterCtrl.awesomeThings.length).toBe(3);
  });
});
