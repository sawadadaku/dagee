'use strict';

describe('Controller: AreaHeaderCtrl', function () {

  // load the controller's module
  beforeEach(module('dageeViewApp'));

  var AreaHeaderCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AreaHeaderCtrl = $controller('AreaHeaderCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(AreaHeaderCtrl.awesomeThings.length).toBe(3);
  });
});
